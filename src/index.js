console.log('hai');

function dogsAge(age) {
  return age * 7;
}
let myDogsAge = dogsAge(4);
console.log(myDogsAge);

function rectanglearea(width, height) {
  let area = width * height;
  return area;
}
let rectarea = rectanglearea(3, 4);
console.log(rectarea);

function addUp(numarray) {
  let total = 0;
  for (let i = 0; i < numarray.length; i++) {
    total += numarray[i];
  }
  return total;
}
let thetotal = addUp([1, 3, 3, 9]);
console.log(thetotal);

function getavatar(points) {
  var avatar;
  if (points < 100) {
    avatar = 'Mouse';
  } else if (points > 100 && points < 1000) {
    avatar = 'Cat';
  } else {
    avatar = 'ape';
  }
  return avatar;
}
let myavatar = getavatar(335);
console.log(myavatar);

let avatar = 'generic';
let skill = 1.0;
let pointsPerLevel = 1000;
let userPoints = 2008;
function getavatarr(points) {
  var level = points / pointsPerLevel;
  if (level == 0) {
    return 'Teddy bear';
  } else if (level == 1) {
    return 'Cat';
  } else if (level >= 2) {
    return 'Gorilla';
  }
}

function updatePoints(bonus, newPoints) {
  for (var i = 0; i < bonus; i++) {
    newPoints += skill * bonus;
  }
  return newPoints + userPoints;
}
userPoints = updatePoints(2, 100);
avatar = getavatar(2112);
console.log(userPoints);
console.log(avatar);
console.log(getavatarr(1000));

function addOne(num) {
  return num + 1;
}
let plusOne = addOne;
let result = plusOne(1);
console.log(result);

let f = function(num) {
  return num + 1;
};
let r = f(1);
console.log(r);

function init() {
  console.log('hello welcome');
}

window.onload = init;

var movie1 = {
  title: 'Plan 9 from Outer Space',
  genre: 'Cult Classic',
  rating: 5,
  showtimes: ['3:00pm', '7:00pm', '11:00pm']
};

var movie2 = {
  title: 'Forbidden Planet',
  genre: 'Classic Sci-fi',
  rating: 5,
  showtimes: ['5:00pm', '9:00pm']
};

function getTimeFromString(timeString) {
  var theTime = new Date();
  var time = timeString.match(/(\d+)(?::(\d\d))?\s*(p?)/);
  theTime.setHours(parseInt(time[1]) + (time[3] ? 12 : 0));
  theTime.setMinutes(parseInt(time[2]) || 0);
  return theTime.getTime();
}

function Movie(title, genre, rating, showtimes) {
  this.title = title;
  this.genre = genre;
  this.rating = rating;
  this.showtimes = showtimes;
  this.getNextShow = function() {
    let now = new Date().getTime();
    for (let i = 0; i < this.showtimes.length; ++i) {
      let showtime = getTimeFromString(this.showTimes[i]);
      if (showtime - now > 0) {
        return 'next show of' + this.title + 'is' + this.showtime[i];
      }
    }
  };
}

console.log(movie1);
console.log(movie2);
let planMovie = new Movie('plan 9 from outer space', 'cult classic', 2, [
  '3.00pm',
  '7.00pm'
]);

console.log(planMovie.getNextShow());
