function makephrases() {
  let words1 = ['always', 'write', 'good', 'code'];
  let words2 = ['find', 'something', 'interesting', 'feature'];
  let words3 = ['never', 'write', 'bad', 'code'];
  let r1 = Math.floor(Math.random() * words1.length);
  let r2 = Math.floor(Math.random() * words2.length);
  let r3 = Math.floor(Math.random() * words3.length);

  let phrase = words1[r1] + '' + words2[r2] + '' + words3[r3];
  let phraseWord = document.getElementById('phrase');
  phraseWord.innerHTML = phrase;
}

window.onload = makephrases;
