function showTemps() {
  let tempByHour = new Array();
  tempByHour[0] = 59.2;
  tempByHour[1] = 60.1;
  tempByHour[2] = 63;
  tempByHour[3] = 65;
  tempByHour[4] = 62;
  for (let i = 0; i < tempByHour.length; ++i) {
    let temp = tempByHour[i];
    let id = 'temp' + i;
    let li = document.getElementById(id);
    if (i == 0) {
      li.innerHTML = 'the temp at noon was' + temp;
    } else {
      li.innerHTML = 'the temp at ' + i + 'was' + temp;
    }
  }
}

window.onload = showTemps;
